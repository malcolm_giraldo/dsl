//This is the pipeline example of DSL script

job('demo') {
    steps {
        shell('echo Hello World!')
    }
}

pipelineJob('github-demo') {
    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        url('https://gitlab.com/malcolm_giraldo/dsl.git')
                    }
                }
            }
           // scriptPath('declarative-examples/simple-examples/environmentInStage.groovy')
        }
    }
}
